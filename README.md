# Projet-IDM

## Objectif du projet :
Créer un DSL pour le pré/post-traitement des données, à utiliser dans les flux de travail centrés sur les données (par exemple, apprentissage automatique, simulations, visualisations

## Concepts DSL :
- Chargement/enregistrement de cadres de données depuis/vers des fichiers CSV et JSON
- Stockage des cadres de données dans des variables
- Ajouter/supprimer/renommer des lignes et des colonnes
- Déplacer/échanger/trier les lignes et les colonnes
- Fusionner/intersecter des cadres de données
## Services DSL :
- Interprète
- Compilateur vers Python

